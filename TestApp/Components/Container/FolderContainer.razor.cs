﻿using Microsoft.AspNetCore.Components;
using TestApp.Models;

namespace TestApp.Components.Container;

public partial class FolderContainer : ComponentBase
{
    [Parameter] public required FolderInfoModel FolderInfoModel { get; set; }
    [Parameter] public string FolderPath { get; set; } = string.Empty;
}