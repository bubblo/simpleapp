﻿namespace TestApp.Models;

public class FolderInfoModel
{
    public Dictionary<string, FolderInfoModel> Subfolders { get; }
    public Dictionary<string, FileInfoModel>  FileInfoList { get; }
    public string Name { get;  }
    public bool WasAnalyzed { get; set; }

    public FolderInfoModel(string name)
    {
        Subfolders = new Dictionary<string, FolderInfoModel>();
        FileInfoList = new Dictionary<string, FileInfoModel>();
        Name = name;
        WasAnalyzed = false;
    }

}