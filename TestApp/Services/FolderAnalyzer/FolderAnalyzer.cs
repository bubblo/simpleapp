﻿using TestApp.Helper;
using TestApp.Models;

namespace TestApp.Services.FolderAnalyzer;

public class FolderAnalyzer : IFolderAnalyzer
{
    private readonly Dictionary<string, FolderInfoModel> _root = new();
    
    public bool IsFolderExist(string directoryPath)
    {
        return Directory.Exists(directoryPath);
    }

    public FolderInfoModel GetFolderFileInfoList(string directoryPath)
    {
        var currentFolder = new DirectoryInfo(directoryPath);
        var currentFoldersNames = currentFolder.FullName.Split(Path.DirectorySeparatorChar).ToList();
        var diskName = currentFoldersNames.First();
        var index = 0;
        currentFoldersNames.RemoveAt(0);

        if (!_root.TryGetValue(diskName, out var folder))
        {
            folder = new FolderInfoModel(diskName);
            _root.Add(diskName, folder);
        }

        var folderForAnalyze = RecursiveAddOrGetFolderModel(folder, currentFoldersNames, index);
        try
        {
            RecursiveAddOrGetSubfoldersModel(folderForAnalyze, directoryPath);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

        return folderForAnalyze;
    }
    
    private FolderInfoModel RecursiveAddOrGetFolderModel(FolderInfoModel folderInfoModel, List<string> folderFullName, int index)
    {
        folderInfoModel.Subfolders.TryGetValue(folderFullName[index], out var subFolder);

        if (subFolder == null)
        {
            var newFolder = new FolderInfoModel(folderFullName[index]);
            folderInfoModel.Subfolders.Add(newFolder.Name, newFolder);
            subFolder = newFolder;
        }

        if (folderFullName.Count - 1 == index)
        {
            return subFolder;
        }

        index++;
        return RecursiveAddOrGetFolderModel(subFolder, folderFullName, index);  
    }

    private void RecursiveAddOrGetSubfoldersModel(FolderInfoModel folderForAnalyze, string directoryPath)
    {
        var isFolderExist = IsFolderExist(directoryPath);
        var currentSubFolders = isFolderExist
            ? Directory.GetDirectories(directoryPath, "*", SearchOption.TopDirectoryOnly)
                .Select(absolutePath =>
                {
                    var pathParts = absolutePath.Split(Path.DirectorySeparatorChar);
                    return new FolderInfoModel(pathParts[^1]);
                })
                .ToList()
            : [];

        var currentFolderFiles = isFolderExist
            ? Directory.GetFiles(directoryPath, "*", SearchOption.TopDirectoryOnly)
                .Select(path => new FileInfo($"{path}"))
                .ToList()
            : [];

        DeleteRemovedFolders(folderForAnalyze.Subfolders, currentSubFolders); // toto ešte nieje asi uplne dobre
        DeleteRemovedFiles(folderForAnalyze);
        
        if (folderForAnalyze.WasAnalyzed)
        {
            ProcessSavedFolder(folderForAnalyze, currentSubFolders, currentFolderFiles);
        }
        else
        {
            ProcessNewFolder(folderForAnalyze, currentSubFolders, currentFolderFiles);
        }
        
        foreach (var subfolder in folderForAnalyze.Subfolders.Values)
        {
            RecursiveAddOrGetSubfoldersModel(subfolder, $"{directoryPath}{Path.DirectorySeparatorChar}{subfolder.Name}");
        }
    }
    
    private void ProcessSavedFolder(FolderInfoModel folderForAnalyze, List<FolderInfoModel> currentSubFolders, List<FileInfo> currentFolderFiles)
    {
        foreach (var folder in currentSubFolders)
        {
            folderForAnalyze.Subfolders.TryAdd(folder.Name, folder);
        }
            
        foreach (var file in currentFolderFiles)
        {
            if (folderForAnalyze.FileInfoList.TryGetValue(file.Name, out var fileInfoModel))
            {
                fileInfoModel.TrySetLastModified(file.LastWriteTime);
            }
            else
            {
                folderForAnalyze.FileInfoList.Add(file.Name, new FileInfoModel(file.Name, file.LastWriteTime));
            }
        }
    }

    private void ProcessNewFolder(FolderInfoModel folderForAnalyze, List<FolderInfoModel> currentSubFolders, List<FileInfo> currentFolderFiles)
    {
        foreach (var folder in currentSubFolders)
        {
            folderForAnalyze.Subfolders.TryAdd(folder.Name, folder);
        }
        foreach (var file in currentFolderFiles)
        {
            folderForAnalyze.FileInfoList.TryAdd(file.Name, new FileInfoModel(file.Name, file.LastWriteTime));  
        }

        folderForAnalyze.WasAnalyzed = true;
    }
    
    private void DeleteRemovedFiles(FolderInfoModel folderForAnalyze)
    {
        foreach (var fileInfoModel in folderForAnalyze.FileInfoList.Values)
        {
            if (fileInfoModel.FileState == FileState.Removed)
            {
                folderForAnalyze.FileInfoList.Remove(fileInfoModel.Name);
            }
            fileInfoModel.FileState = FileState.Removed;
        }
    }
    
    private void DeleteRemovedFolders(Dictionary<string, FolderInfoModel> folderInfoModels, List<FolderInfoModel> newFolderInfoModels)
    {
        var subfolderNames = newFolderInfoModels.Select(e => e.Name);
        foreach (var keyValuePair in folderInfoModels.Where(e => !subfolderNames.Contains(e.Key)))
        {
            if (folderInfoModels[keyValuePair.Key].FileInfoList.Count == 0)
            {
                folderInfoModels.Remove(keyValuePair.Key);
            }
        }
    }
    
}